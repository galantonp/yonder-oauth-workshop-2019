package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfigurer implements AuthorizationServerConfigurer {

  @Autowired
  private AuthenticationManager authManager;

  @Autowired
  private UserDetailsService userDetailsService;

  @Override
  public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
    // no extra configuration for the /oauth/token endpoint needed
    // by default basic authentication is required for confidential clients
    security.checkTokenAccess("isAuthenticated()");
  }

  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    // @formatter:off
    clients.inMemory()
      .withClient("client1")
        .secret("{noop}123")
        .accessTokenValiditySeconds(300)
        .refreshTokenValiditySeconds(3600) // <= 0 for non-expiring refresh tokens
        .authorizedGrantTypes("authorization_code", "implicit", "password", "client_credentials", "refresh_token")
        .redirectUris("http://localhost:8080/spring-client-server/oauth/callback")
        .scopes("products.read", "orders.read", "orders.write")
        .autoApprove(false) // <= true if consent from user is not required
        .and()
      .withClient("resourceServer")
        .secret("{noop}123");
      
    // @formatter:on
  }

  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    // register an authentication manager to enable password grants
    endpoints //
        .authenticationManager(authManager) //
        .reuseRefreshTokens(false) // single-use refresh tokens
        .userDetailsService(userDetailsService);
  }

}
