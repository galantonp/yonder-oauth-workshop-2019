package com.example.demo.repository.memory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.example.demo.model.Product;
import com.example.demo.repository.ProductRepository;

public class InMemoryProductRepository implements ProductRepository {

  private List<Product> products = new ArrayList<>();

  public InMemoryProductRepository() {
    products.add(new Product(1, "Orange", BigDecimal.valueOf(3)));
    products.add(new Product(2, "Apple", BigDecimal.valueOf(3)));
  }

  @Override
  public List<Product> findAll() {
    return Collections.unmodifiableList(products);
  }
}
