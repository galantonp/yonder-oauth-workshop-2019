package com.example.demo.repository.memory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.Validate;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;
import com.example.demo.repository.OrderRepository;

public class InMemoryOrderRepository implements OrderRepository {

  private long nextId = 1;
  private Map<Long, Order> orders = new HashMap<>();
  
  @Override
  public Order create(String username, List<OrderItem> items) {
    Validate.notNull(items);
    
    Order order = new Order(nextId++, username, new Date());
    for (OrderItem item : items) {
      order.getItems().add(item);
    }
    orders.put(order.getId(), order);
    
    return order;
  }

  @Override
  public List<Order> findAll(String username) {
    Validate.notNull(username);
    
    return orders.values().stream() //
        .filter(order -> username.equals(order.getUsername()))
        .collect(Collectors.toList());
  }
  
  @Override
  public Order find(long orderId) {
    return orders.get(orderId);
  }

}
