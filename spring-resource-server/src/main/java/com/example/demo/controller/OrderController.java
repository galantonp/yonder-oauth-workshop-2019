package com.example.demo.controller;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;
import com.example.demo.repository.OrderRepository;

@RestController
public class OrderController {

  private OrderRepository repository;
  
  public OrderController(OrderRepository repository) {
    this.repository = repository;
  }

  @PostMapping("/orders")
  @ResponseStatus(HttpStatus.CREATED)
  @PreAuthorize("hasRole('customer') and #oauth2.hasScope('orders.write')")
  public Order placeOrder(Authentication authentication, @RequestBody List<OrderItem> items) {
    return repository.create(authentication.getName(), items);
  }
  
  @GetMapping("/orders")
  @PreAuthorize("hasAnyRole('customer', 'employee') and #oauth2.hasScope('orders.read')")
  public List<Order> listOders(Authentication authentication) {
    return repository.findAll(authentication.getName());
  }
  
  @GetMapping("/orders/{orderId}")
  @PreAuthorize("hasAnyRole('customer', 'employee') and #oauth2.hasScope('orders.read')")
  public Order orderDetails(@PathVariable long orderId) {
    // TODO allow access only to the user's own orders
    return repository.find(orderId);
  }
}
