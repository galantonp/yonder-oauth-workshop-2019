package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Product;
import com.example.demo.repository.ProductRepository;

@RestController
public class ProductController {

  @Autowired
  private ProductRepository repository;

  @GetMapping("/products")
  @PreAuthorize("#oauth2.hasScope('products.read')")
  public List<Product> listProducts() {
    return repository.findAll();
  }

}
