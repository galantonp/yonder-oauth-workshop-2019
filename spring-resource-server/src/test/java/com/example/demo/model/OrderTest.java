package com.example.demo.model;

import static org.junit.Assert.assertEquals;
import java.math.BigDecimal;
import java.util.Date;
import org.junit.Test;

public class OrderTest {

  @Test
  public void testTotalPrice() {
    Order order = new Order(1, "petru", new Date());
    order.getItems().add(new OrderItem(new Product(1, "Orange", BigDecimal.valueOf(3)), 14));
    order.getItems().add(new OrderItem(new Product(2, "Apple", BigDecimal.valueOf(2)), 5));
    
    BigDecimal totalPrice = order.getTotalPrice();
    assertEquals(BigDecimal.valueOf(52), totalPrice);
  }
}
