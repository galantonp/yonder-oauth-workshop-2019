package com.example.demo.config;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.resource.UserRedirectRequiredException;
import org.springframework.security.oauth2.client.token.AccessTokenProviderChain;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import com.example.demo.oauth.RevokeTokenLogoutHandler;

@Configuration
@EnableWebSecurity
@EnableOAuth2Client
public class OAuthClientConfigurer extends WebSecurityConfigurerAdapter {

  public static final String CLIENT_ID = "client1";
  public static final String CLIENT_SECRET = "123";
  public static final String BASE = "http://localhost:8080/spring-auth-server/oauth";
  public static final String AUTHORIZATION_ENDPOINT = BASE + "/authorize";
  public static final String TOKEN_ENDPOINT = BASE + "/token";
  public static final String TOKEN_INTROSPECTION_ENDPOINT = BASE + "/check_token";
  public static final String TOKEN_REVOCATION_ENDPOINT = BASE + "/revoke_token";
  public static final String AUTHORIZATION_CALLBACK_PATH = "/oauth/callback";
  public static final String REDIRECT_ENDPOINT =
      "http://localhost:8080/spring-client-server" + AUTHORIZATION_CALLBACK_PATH;


  @Autowired
  private OAuth2ClientContext oauth2ClientContext;

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/logged_out");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    // @formatter:off
    http.antMatcher("/**")
      .csrf().disable()
      .authorizeRequests()
        .anyRequest().authenticated()
        .and()
      .logout()
        .clearAuthentication(true)
        .invalidateHttpSession(true)
        .logoutUrl("/logout")
        .logoutSuccessUrl("/logged_out")
//        .addLogoutHandler(new RevokeTokenLogoutHandler(oauthRestTemplate()))
        .and()
      .addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class)
      .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint());
    // @formatter:on
  }

  @Bean
  public AuthorizationCodeResourceDetails authServer() {
    AuthorizationCodeResourceDetails resource = new AuthorizationCodeResourceDetails();
    resource.setUserAuthorizationUri(AUTHORIZATION_ENDPOINT);
    resource.setAccessTokenUri(TOKEN_ENDPOINT);
    resource.setClientAuthenticationScheme(AuthenticationScheme.header);
    resource.setClientId(CLIENT_ID);
    resource.setClientSecret(CLIENT_SECRET);
    resource.setPreEstablishedRedirectUri(REDIRECT_ENDPOINT);
    resource.setScope(Arrays.asList("products.read", "orders.read", "orders.write"));

    return resource;
  }

  @Bean
  public OAuth2RestTemplate oauthRestTemplate() {
    AuthorizationCodeAccessTokenProvider provider = new AuthorizationCodeAccessTokenProvider();
    provider.setStateMandatory(false);

    OAuth2RestTemplate template = new OAuth2RestTemplate(authServer(), oauth2ClientContext);
    template.setAccessTokenProvider(new AccessTokenProviderChain(Arrays.asList(provider)));
    template.setRetryBadAccessTokens(false);

    return template;
  }

  private Filter ssoFilter() {
    OAuth2ClientAuthenticationProcessingFilter oauthFilter =
        new OAuth2ClientAuthenticationProcessingFilter(AUTHORIZATION_CALLBACK_PATH);
    oauthFilter.setRestTemplate(oauthRestTemplate());
    oauthFilter.setTokenServices(tokenServices());

    return oauthFilter;
  }

  public ResourceServerTokenServices tokenServices() {
    RemoteTokenServices tokenServices = new RemoteTokenServices();
    tokenServices.setCheckTokenEndpointUrl(TOKEN_INTROSPECTION_ENDPOINT);
    tokenServices.setClientId(CLIENT_ID);
    tokenServices.setClientSecret(CLIENT_SECRET);

    return tokenServices;
  }

  public AuthenticationEntryPoint authenticationEntryPoint() {
    return new AuthenticationEntryPoint() {

      @Override
      public void commence(HttpServletRequest request, HttpServletResponse response,
          AuthenticationException authException) throws IOException, ServletException {

        String redirectUri = AUTHORIZATION_ENDPOINT;
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("response_type", "code");
        requestParams.put("client_id", CLIENT_ID);

        throw new UserRedirectRequiredException(redirectUri, requestParams);
      }
    };
  }

}
