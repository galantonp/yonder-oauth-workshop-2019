package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DebugController {

	@Autowired
	private OAuth2RestTemplate restTemplate;

	@GetMapping(path = "/debug", produces = "application/json")
	public DebugResponse debug() {
		OAuth2AccessToken accessToken = restTemplate.getAccessToken();
		return new DebugResponse(accessToken.getValue(), accessToken.getRefreshToken().getValue());
	}

	public static class DebugResponse {

		private final String accessToken;
		private final String refreshToken;

		public DebugResponse(String accessToken, String refreshToken) {
			this.accessToken = accessToken;
			this.refreshToken = refreshToken;
		}

		public String getAccessToken() {
			return accessToken;
		}

		public String getRefreshToken() {
			return refreshToken;
		}
		
	}
}
