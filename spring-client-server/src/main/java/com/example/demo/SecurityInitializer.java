package com.example.demo;

import javax.servlet.ServletContext;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.filter.DelegatingFilterProxy;

/**
 * Servlet API 3.1 initializer which registers the Spring Security filter.
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {

  @Override
  protected void beforeSpringSecurityFilterChain(ServletContext servletContext) {
    DelegatingFilterProxy filter = new DelegatingFilterProxy("oauth2ClientContextFilter");
    servletContext.addFilter("oauthClientFilter", filter).addMappingForUrlPatterns(null, false, "/*");
  }
}
